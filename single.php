<?php get_header(); ?>
            <div id="innerpage" class="theblog">
			<a onclick="history.go(-1);return false;" href="#" style="position:relative;top:-15px;" class="prevbtn">&laquo; go to previous page</a>
			<?php while (have_posts()) : the_post(); ?>
			<h1><?php the_title(); ?></h1>
			<?php the_content(); ?>
            <div class="clear"></div>
            <?php endwhile; ?>
            <div class="clear"></div>
            </div><!--/left-->
    	</div> <!--/center-->
    </div> <!--/main-->
<?php get_footer(); ?>

