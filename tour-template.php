<?php
/*
Template Name: Tour Template
*/
?>
<?php get_header(); ?>
            <div id="innerpage">                    <div style="clear:both;">						<iframe id="ytplayer" width="560" height="315" src="//www.youtube.com/embed/gH-2jzEAOKo?rel=0&showinfo=0&autohide=1&modestbranding=0&color1=ffffff" frameborder="0" allowfullscreen></iframe>						<br />                    </div>						
                    <?php while (have_posts()) : the_post(); ?>
                        <div style="float:left;">
                        <img src="<?php bloginfo('template_url'); ?>/images/floor-plan.png" class="slide-img map" alt="Slide 1" usemap="#simple" />
                          <map name="simple" id="first">
                            <!--<area alt="Make Up room" nohref="<?php bloginfo('template_url'); ?>/images/floor-plans/makeup.jpeg" shape="rect" coords="214,383,368,496" href="#" />-->
                            <area alt="Shower" nohref="<?php bloginfo('template_url'); ?>/images/floor-plans/showertouchedup.jpg" shape="rect" coords="367,435,399,496" href="#" />
                            <area alt="Viewing Room" nohref="<?php bloginfo('template_url'); ?>/images/floor-plans/viewing-room.jpg" shape="poly" coords="310,571,310,592,321,592,319,601,310,607,399,607,399,571,310,571" href="#" />
                            <area alt="Studio Floor" nohref="<?php bloginfo('template_url'); ?>/images/floor-plans/studio.jpeg" shape="poly" coords="41,107,41,366,60,366,60,408,75,404,88,395,96,379,98,366,101,381,107,393,121,404,137,408,136,367,157,367,191,358,214,380,399,380,399,107" href="#" />
                            <!--<area alt="Stairs" nohref="" shape="poly" coords="155,368,189,360,211,383,211,494,155,495" href="#" />-->
                            <area alt="Green Room" nohref="<?php bloginfo('template_url'); ?>/images/image1.jpg" shape="poly" coords="307,572,307,608,307,660,307,691,217,690,217,654,179,654,179,598,191,596,199,588,206,576,182,575,185,572" href="#" />
                            <!--<area alt="WC" nohref="<?php bloginfo('template_url'); ?>/images/floor-plans/toilet.jpg" shape="rect" coords="155,656,212,691" href="#" />-->
                            <!--<area alt="Stairs" nohref="" shape="poly" coords="205,575,199,589,191,596,176,598,176,637,156,637,156,573,183,573,182,576" href="#" />-->
                            <area alt="Vehicle Access" nohref="<?php bloginfo('template_url'); ?>/images/floor-plans/Vehicle-Access-touched-up.jpg" shape="poly" coords="42,370,56,370,56,411,65,409,75,406,84,399,93,390,98,375,103,390,114,400,122,406,133,409,141,410,141,369,151,369,151,496,141,489,134,491,129,493,118,489,113,492,107,493,101,491,96,489,90,492,85,494,78,490,75,489,70,491,66,493,62,494,59,492,51,490,47,488,42,493" href="#" />
                            <area alt="Lighting Store" nohref="<?php bloginfo('template_url'); ?>/images/floor-plans/lightingstoretouchedup.jpg" shape="rect" coords="41,572,154,619" href="#" />
                            <area alt="Studio Manager" nohref="<?php bloginfo('template_url'); ?>/images/floor-plans/studio-manager.jpg" shape="poly" coords="42,620,42,690,150,690,150,651,176,652,177,640,152,640,152,620" href="#" />
                            <area alt="Kitchen Area" nohref="<?php bloginfo('template_url'); ?>/images/floor-plans/kitchenarea.jpg" shape="rect" coords="308,607,400,692" href="#" />
                            <area alt="Make Up room" nohref="<?php bloginfo('template_url'); ?>/images/floor-plans/makeup.jpeg" shape="poly" coords="214,385,214,449,242,448,242,496,368,496,366,436,400,436,400,416,366,416,367,385" href="#" />
                            <!--<area alt="WC" shape="rect" nohref="<?php bloginfo('template_url'); ?>/images/floor-plans/toilet.jpg" coords="214,450,242,497" href="#" />-->
                          </map>
                        
						<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery-1.8.2.min.js"></script>
						<script>
                        $(document).ready(function(){
                            $('#first area').imgPreview({
                                containerID: 'imgPreviewWithStyles',
                                imgCSS: {
                                    // Limit preview size:
                                    width: 400
                                },
                                // When container is shown:
                                onShow: function(link){
                                    // Animate link:
                                    $(link).stop().animate({opacity:0.4});
                                    // Reset image:
                                    $('img', this).stop().css({opacity:0});
                                    // Title Below			
                                    $('<span class="IMGcaption">' + $(link).attr("alt") + '</span>').appendTo(this);
                            
                                },
                                // When image has loaded:
                                onLoad: function(){
                                    // Animate image
                                    $(this).animate({opacity:1}, 800);
                                },
                                // When container hides: 
                                onHide: function(link){
                                    // Animate link:
                                    $(link).stop().animate({opacity:1});
                                    // Title Below 
                                    $('span', this).remove();	
                                }
                            
                            });					
                        });
                        </script>
                        </div>
                        <div style="float:left;width:200px;" class="mob100"><?php the_content(); ?></div>
                        <div class="clear"></div>
                    <?php endwhile; ?>
            </div>
    	</div> <!--/center-->
    </div> <!--/main-->
<?php get_footer(); ?>