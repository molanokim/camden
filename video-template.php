<?php
/*
Template Name: Video Template
*/
?>
<?php get_header(); ?>
            <div id="innerpage">
				<?php while (have_posts()) : the_post(); ?>
                    <?php the_content(); ?>
                    <div class="clear"></div>
                    <ul class="showreel-categories"><h1>Looking for Something Specific?</h1>
                        <li><a href="http://thecamdenstudio.com/commercial-reels/">Commercials made at Camden Studio</a></li>
                        <li><a href="http://thecamdenstudio.com/feature-film-reels/">Feature Films made at Camden Studio</a></li>
                        <li><a href="http://thecamdenstudio.com/music-video-reels/">Music Videos made at Camden Studio</a></li>
                        <li><a href="http://thecamdenstudio.com/tv-reels/">TV made at Camden Studio</a></li>
                    </ul>
                <?php endwhile; ?>
            </div>
    	</div> <!--/center-->
    </div> <!--/main-->
<?php get_footer(); ?>