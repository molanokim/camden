<?php
/*
Template Name: Blog Template
*/
?>
<?php get_header(); ?>
            <div id="innerpage">
				<div id="blog-innerpage">
				<?php //query_posts('showposts=-1&cat=-34,-37,-35,-36,-5,-1'); ?>
				<?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
					query_posts(array('post_type' => 'post', 'types' => $types, 'posts_per_page' => 10, 'paged' => $paged, 'cat' => '-34,-37,-35,-36,-5,-1' ) ); ?>				
                <?php while (have_posts()) : the_post(); ?>
                        <div class="blog-content">
                        <h1><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h1>
                        <small>Category: <?php the_category(', ') ?></small><br class="spacer" />
                        <?php if ( function_exists('has_post_thumbnail') && has_post_thumbnail() ) { the_post_thumbnail(array(150,150), array('class' => 'alignleft-thumb')); } ?>
						<?php $excerpt = get_the_excerpt(); echo string_limit_words($excerpt,45); ?>
                        <a class="continue-reading" href="<?php the_permalink(); ?>"><i>read more...</i></a>
                        <div class="clear"></div>
						</div>
                        <div class="clear"></div>
                <?php endwhile;?>
				<?php paginate(); ?>
                </div>
                <?php get_sidebar(); ?>
            <div class="clear"></div>
            </div><!-- inner page -->
    	</div> <!--/center-->
    </div> <!--/main-->
<?php get_footer(); ?>

