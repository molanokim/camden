<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php bloginfo('name'); ?></title>
<meta name="geo.region" content="GB" />
<meta name="geo.placename" content="London" />
<meta name="geo.position" content="51.539678;-0.13556" />
<meta name="ICBM" content="51.539678, -0.13556" />
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="alternate" type="application/atom+xml" title="<?php bloginfo('name'); ?> Atom Feed" href="<?php bloginfo('atom_url'); ?>" />
<link rel="shortcut icon" href="<?php bloginfo('stylesheet_directory'); ?>/favicon.png" />
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/style.css"/>
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/reset.css"/>
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/newsletter.css"/>
<!--[if IE 6]><link type="text/css" rel="stylesheet" href="<?php bloginfo('template_url')?>/ie6.css" /><![endif]-->  
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/mailing-list.js"></script>


<?php wp_head(); ?>
</head>

<body>
<div id="wrapper">

	<div id="header">
    	<div class="center">
        <div id="logo">
        <a href="<?php bloginfo('home'); ?>">
        <img alt="camden studio logo" src="<?php bloginfo('template_url'); ?>/images/camden_studio_logo.jpg" /></a>
        </div><!-- end logo -->
        <div id="contact">
        Unit 6, St. Pancras Commercial Centre<br />
        63 Pratt Street, London<br />
        NW1 0BY<br />
        <span>Call Now: 020 7419 0567</span>
        </div><!-- end contact-->        
        </div><!-- end center -->
    </div>

    <div id="main">
        <div class="center">
            <div id="menu">
                <ul id="nav" class="drop">
					<?php wp_nav_menu( array('menu' => 'Main Menu Navigation', 'container' => 'none' )); ?>
					<?php if (is_category() || (is_page('Blog'))) { ?>
                        <li id="menu-item-blog"><a href="<?php bloginfo('home'); ?>/blog/" style="color:#6BC89C;">Blog</a>
                    <?php } else { ?>
                        <li id="menu-item-blog"><a href="<?php bloginfo('home'); ?>/blog/" >Blog</a>
                    <?php } ?>
                    	<!--<ul>
							<?php //wp_list_categories('orderby=name&title_li=&hide_empty=0&exclude=1&current_category='.$myCat); ?> 
                        </ul>-->
                    </li>
                </ul>
            </div><!--/menu-->
            <div style="clear:both;"></div>	