<?php
/*
Template Name: Front Page Template
*/
?>
<?php get_header(); ?>
            <div class="slider-wrapper theme-default">
					<?php echo do_shortcode('[smoothslider id="1"]'); ?>
			</div><!--/SLIDER-->
    
            <br /><br />
         <div class="homepage">
            <div class="left" style="margin-top:-15px;">
			<?php while (have_posts()) : the_post(); ?>
            <?php the_content(); ?>
            <div class="clear"></div>
            <?php endwhile; ?>
            </div><!--/left-->
    
            <div class="right">
                <div class="box">
                    <!--<div id="slider1" style="width: 365px;">
						<?php query_posts('cat=5'); while (have_posts()) : the_post(); ?>                        <div style="width: 340px;"><strong><?php the_title(); ?></strong><br /><?php echo $wpdb->get_var("SELECT post_content FROM $wpdb->posts WHERE ID = $id");?></div>                        <?php endwhile; ?>  
                    </div>--><!--/slider1-->

		<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('sidebar-slider') ) : ?><?php endif; ?>
		
                </div><!--/box-->
                <div id="newsletter">
	                <div class="newsletter-text">Join our client list to receive preferential discounts.</div>
                        <form id="signup" action="<?=$_SERVER['PHP_SELF']; ?>" method="get">
                            <fieldset>
                                <input type="text" name="email" id="email" class="newsletter-input" />
                                <input type="image" src="<?php bloginfo('template_url'); ?>/images/newsletter-button.jpg" name="submit" value="Join" class="btn" alt="Join" />
                                <span id="response"><? require_once('inc/store-address.php'); if($_GET['submit']){ echo storeAddress(); } ?></span>
                            </fieldset>
                        </form>      
                        
                    <div class="clear"></div>
                </div>
                <br /><br />
            </div><!--/right-->
</div><!-- end homepage div -->
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery-1.8.2.min.js"></script><script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.bxSlider.min.js"></script>
<script type="text/javascript">
$('#slider').bxSlider({
	auto: true,
	controls: false,
	speed: 1000,
	pause: 6000
});
</script>
            
    	</div> <!--/center-->
    </div> <!--/main-->

<?php get_footer(); ?>