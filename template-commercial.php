<?php
/*
Template Name: Show Reels Template
*/
?>
<?php get_header(); ?>

            <div id="innerpage">
            <a onclick="history.go(-1);return false;" href="#" style="position:relative;top:-15px;">&laquo; go to previous page</a>
				<?php if ( is_page('commercials-reels') ) { ?>
					<h1>Featured Commercials Video</h1>
                <?php } if ( is_page('music-video-reels') ) { ?>
					<h1>Featured Music Video</h1>
                <?php } if ( is_page('tv-reels') ) { ?>
					<h1>Featured TV Video</h1>
                <?php } if ( is_page('feature-film-reels') ) { ?>
					<h1>Featured Film Video</h1>   
                <?php } ?>				 
				 <?php if (have_posts()) : ?>
                   <?php while (have_posts()) : the_post(); ?>    
                   <?php the_content(); ?>
                   <?php endwhile; ?>
                 <?php endif; ?>

				<?php if ( is_page('commercial-reels') ) { ?>
                    <ul class="showreel-categories" style="margin-bottom:17px;">
                        <li><a href="http://thecamdenstudio.com/feature-film-reels/">Feature Films made at Camden Studio</a></li>
                        <li><a href="http://thecamdenstudio.com/music-video-reels/">Music Videos made at Camden Studio</a></li>
                        <li><a href="http://thecamdenstudio.com/tv-reels/">TV made at Camden Studio</a></li>
                    </ul>
					<h1 style="text-align:center;">You are now viewing Commercials made at Camden Studio</h1>
                    <?php query_posts('showposts=-1&cat=30'); ?>
                <?php } if ( is_page('music-video-reels') ) { ?>
                    <ul class="showreel-categories" style="margin-bottom:17px;">
                        <li><a href="http://thecamdenstudio.com/commercial-reels/">Commercials made at Camden Studio</a></li>
                        <li><a href="http://thecamdenstudio.com/feature-film-reels/">Feature Films made at Camden Studio</a></li>
                        <li><a href="http://thecamdenstudio.com/tv-reels/">TV made at Camden Studio</a></li>
                    </ul>				
                    <h1 style="text-align:center;">You are now viewing Music Videos made at Camden Studio</h1>
                    <?php query_posts('showposts=-1&cat=32'); ?>
                <?php } if ( is_page('tv-reels') ) { ?>
                    <ul class="showreel-categories" style="margin-bottom:17px;">
                        <li><a href="http://thecamdenstudio.com/commercial-reels/">Commercials made at Camden Studio</a></li>
                        <li><a href="http://thecamdenstudio.com/feature-film-reels/">Feature Films made at Camden Studio</a></li>
                        <li><a href="http://thecamdenstudio.com/music-video-reels/">Music Videos made at Camden Studio</a></li>
                    </ul>				
                    <h1 style="text-align:center;">You are now viewing TV shows made at Camden Studio</h1>
                    <?php query_posts('showposts=-1&cat=33'); ?>
                <?php } if ( is_page('feature-film-reels') ) { ?>
                    <ul class="showreel-categories" style="margin-bottom:17px;">
                        <li><a href="http://thecamdenstudio.com/commercial-reels/">Commercials made at Camden Studio</a></li>
                        <li><a href="http://thecamdenstudio.com/music-video-reels/">Music Videos made at Camden Studio</a></li>
                        <li><a href="http://thecamdenstudio.com/tv-reels/">TV made at Camden Studio</a></li>
                    </ul>
                    <h1 style="text-align:center;">You are now viewing Feature Films made at Camden Studio</h1>
                    <?php query_posts('showposts=-1&cat=31'); ?> 
                <?php } ?>



                <?php while (have_posts()) : the_post(); ?>
                        <div class="video-content">
						<h1><a title="<?php the_title(); ?>" href="<?php the_permalink(); ?>"><?php  if (strlen($post->post_title) > 35) { echo substr(the_title($before = '', $after = '', FALSE), 0, 35) . '...'; } else { the_title(); } ?> </a></h1>
                        <small>Category: <?php the_category(', ') ?></small><br class="video-spacer"/>
                        <?php
						if ( has_post_thumbnail() ) {
						  the_post_thumbnail(array(80,80), array('class' => 'alignleft'));
						} else {
							 $values = get_post_custom_values("fv_video_id");
								if ( is_array($values) ) {
								echo "<img src=\"http://img.youtube.com/vi/";
								echo $values[0];
								echo "/1.jpg\">";
								} else {
								echo "no image<br />available"; //will be blank if there is no custom fields in featured video
							} 
						}
						?>
						<?php $excerpt = get_the_excerpt(); echo string_limit_words($excerpt,13); ?> <a class="continue-reading" href="<?php the_permalink(); ?>"><i>read more</i></a>
                        <div class="clear"></div>
                        </div>
                <?php endwhile;?>  

            </div>
    	</div> <!--/center-->
    </div> <!--/main-->
<?php get_footer(); ?>