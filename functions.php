<?php 
if ( function_exists( 'add_theme_support' ) ) { 
add_theme_support( 'post-thumbnails' ); 
}add_image_size( 'thumb70', 70, 70, true);
add_action( 'init', 'register_my_menus' );
function register_my_menus() {
register_nav_menus(
array(
'menu-1' => __( 'Menu 1' ),
'menu-2' => __( 'Menu 2' )
));
}
function remove_ul ( $menu ){    return preg_replace( array( '#^<ul[^>]*>#', '#</ul>$#' ), '', $menu );}add_filter( 'wp_nav_menu', 'remove_ul' );add_filter('widget_categories_args','show_empty_categories_links');function show_empty_categories_links($args) {	$args['hide_empty'] = 0;	$args['exclude'] = 1;	return $args;}
function trim_excerpt($text) {
  return rtrim($text,'[...]');
}
add_filter('get_the_excerpt', 'trim_excerpt');
function string_limit_words($string, $word_limit)
{
  $words = explode(' ', $string, ($word_limit + 1));
  if(count($words) > $word_limit)
  array_pop($words);
  return implode(' ', $words);
}
function exclude_widget_categories($args){
$exclude = "34,37,35,36,5,1"; // The IDs of the excluding categories
$args["exclude"] = $exclude;
return $args;
}
add_filter("widget_categories_args","exclude_widget_categories");
if ( function_exists('register_sidebar') ) {
    register_sidebar(array(
		'name' => __( 'Our Guarantee' ),
		'id' => 'our-guarantee',
		'description' => __( 'Guarantee widget' ),
		'before_widget' => '<div class="guarantee">',
		'after_widget' => '</div>',
		'before_title' => '<h1>',
		'after_title' => '</h1>',        
    ));    register_sidebar(array(
		'name' => __( 'sidebar-widget' ),		
		'id' => 'sidebar-widget',		
		'description' => __( 'sidebar-widget' ),		
		'before_widget' => '<div class="widgetize">',		
		'after_widget' => '</div>', 
		'before_title' => '<h1>', 
		'after_title' => '</h1>', 
	));
    register_sidebar(array(
		'name' => __( 'flickr' ),
		'id' => 'flickr',
		'description' => __( 'flickr' ),
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '<h2>',
		'after_title' => '</h2>',        
    ));register_sidebar(array(
		'name' => __( 'Sidebar Slider' ),
		'id' => 'sidebar-slider',
		'description' => __( 'Sidebar Slider' ),
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '',
		'after_title' => '',        
    ));
}
function paginate() {
	global $wp_query, $wp_rewrite;
	$wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;
	
	$pagination = array(
		'base' => @add_query_arg('page','%#%'),
		'format' => '',
		'total' => $wp_query->max_num_pages,
		'current' => $current,
		'show_all' => true,
		'type' => 'list',
		'next_text' => '&raquo;',
		'prev_text' => '&laquo;'
		);
	
	if( $wp_rewrite->using_permalinks() )
		$pagination['base'] = user_trailingslashit( trailingslashit( remove_query_arg( 's', get_pagenum_link( 1 ) ) ) . 'page/%#%/', 'paged' );
	
	if( !empty($wp_query->query_vars['s']) )
		$pagination['add_args'] = array( 's' => get_query_var( 's' ) );
	
	echo paginate_links( $pagination );
}
?>