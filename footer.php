    <div id="footer">
        <div class="center">
            <img src="<?php bloginfo('template_url'); ?>/images/aimimage_logo_black.jpg" alt="aimimage logo"/>
            <img src="<?php bloginfo('template_url'); ?>/images/camden_studio_logo_small.jpg" alt="camden studio"/>
            &nbsp;&nbsp;<img src="<?php bloginfo('template_url'); ?>/images/ICE_logo.jpg" alt="ICE logo"/>
<div class="clearfix"></div>
            <br />Unit 6, St. Pancras Commercial Centre, 63 Pratt Street, London , NW1 0BY TEL: 020 7419 0567 <a href="http://thecamdenstudio.com/terms-conditions/" target="_self">Terms &amp; Conditions</a> | <a href="http://thecamdenstudio.com/our-team/" target="_self">Our Team</a>
        </div><!--/center-->
        <div class="support-section">
            <div id='support-content'>
                Supported by <a href="http://businessstartupsupport.co.uk/" target="_blank">Start Up NOW</a>
            </div>
            <div id='support-button'><img src="<?php bloginfo('template_url'); ?>/images/gray-arrow.png" alt="" /></div>
        </div>
    </div><!--/footer-->

</div>

<div id="slideout">
<div id="clickme">Review Us</div>
    <div id="slidecontent">
        <ul>
            <li><a href="#" target="_blank">Google +</a></li>
            <li><a href="#" target="_blank">yellowpages.ny1.com</a></li>
            <li><a href="#" target="_blank">newyork.citysearch.com</a></li>
            <li><a href="#" target="_blank">yelp.co.uk</a></li>
            <li><a href="#" target="_blank">nymag.com</a></li>
        </ul>
    </div>
</div>

<?php wp_footer(); ?>


<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/tabcontent.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/script.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.maphilight.js"></script>
<script src="<?php bloginfo('stylesheet_directory'); ?>/js/imgpreview.full.jquery.js" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
jQuery("html").addClass("smooth_slider_fouc");jQuery(document).ready(function() {   jQuery(".smooth_slider_fouc #smooth_slider_1").css({"display" : "block"}); });jQuery(document).ready(function() {
                jQuery("#smooth_slider_1").cycle({ 
                        fx: "fade",
                        speed:"500",
                        timeout: "7000",pager: "#smooth_slider_1_nav",pagerAnchorBuilder: function(idx, slide) { 
                                        return '<a class="sldr'+(idx+1)+' smooth_slider_nnav" href="#"><\/a>'; 
                                }, pause: 1
                        ,slideExpr: "div.smooth_slideri"
                });});
//]]>
</script>
</body>
</html>