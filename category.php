<?php get_header(); ?>
            <div id="innerpage">
				<div id="blog-innerpage">
                <?php while (have_posts()) : the_post(); ?>
                        <div class="blog-content">
                        <h1><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h1>
                        <small>Category: <?php the_category(', ') ?></small><br/>
                        <?php the_excerpt(); ?>
                        <a class="continue-reading" href="<?php the_permalink(); ?>"><i>read more...</i></a>
                        </div>
                        <div class="clear"></div>
                        
                <?php endwhile;?>  
                </div>
                <?php get_sidebar(); ?>
            <div class="clear"></div>
            </div><!-- inner page -->
    	</div> <!--/center-->
    </div> <!--/main-->
<?php get_footer(); ?>